# 儿童绘本阅读前台

#### 介绍
使用uniapp搭建前台,调用微信开放接口,请求后台数据进行渲染.

#### 软件架构
软件架构说明


#### 安装教程

1.  npm install 
2.  xxxx
3.  xxxx

#### 使用说明

1.  在uniapp中可运行至浏览器 手机 或微信小程序
2.  若需要请求后台数据,request的接口路径需改为后台所在电脑的IPV4地址
3.  本产品已在微信小程序上线,开源只为方便学习,不可用于任何其他用途,如若发现必严肃处理.
4.  若有其他问题可留言

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
